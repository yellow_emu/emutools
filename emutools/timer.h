#pragma once

#include <chrono>

namespace emutools
{
    class timer
    {
    private:
        using Clock = std::chrono::steady_clock;
        using Second = std::chrono::duration<double, std::ratio<1> >;

        std::chrono::time_point<Clock> _ref { Clock::now() };

    public:
        void reset()
        {
            _ref = Clock::now();
        }

        double elapsed() const
        {
            return std::chrono::duration_cast<Second>(Clock::now() - _ref).count();
        }
    };
}

