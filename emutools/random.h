#pragma once

#include <random>
#include <type_traits>
#include <cassert>

namespace emutools
{
    template<typename T>
    class random
    {   
        using DistroT = std::conditional_t<
            std::is_same_v<T, float> || std::is_same_v<T, double> || std::is_same_v<T, long double>,
            std::uniform_real_distribution<T>,
            std::uniform_int_distribution<T>
        >;

    public:
        random(T min, T max)
        {
            assert(max > min && "max has to be greater than min");
            _distro = DistroT(min, max);
        }
        T generate()    
        {
            return _distro(generator);
        }
    private:
        DistroT _distro;
        
        static std::mt19937 initGenerator()
        {
            std::random_device rd;
            std::seed_seq ss{ rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd() };
            std::mt19937 mt{ ss };
            return mt;
        }
        static inline std::mt19937 generator{initGenerator()};
    };
}